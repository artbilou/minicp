/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2017. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.examples;

import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.search.DFSearch;
import minicp.search.SearchStatistics;
import minicp.util.InconsistencyException;
import minicp.util.InputReader;

import static minicp.cp.Factory.*;
import static minicp.cp.Heuristics.firstFail;
import static minicp.search.Selector.*;

import java.util.ArrayList;
import java.util.List;

public class QAP {

    public static void main(String[] args) throws InconsistencyException {
    	
    	class Pair {
        	public int i;
        	public int j;

        	public Pair(int i, int j) {
        		this.i = i;
        		this.j = j;
        	}
        }

        // ---- read the instance -----

        InputReader reader = new InputReader("data/qap.txt");

        int n = reader.getInt();
        // Weights
        int [][] w = new int[n][n];
        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n; j++) {
                w[i][j] = reader.getInt();
            }
        }
        // Distance
        int [][] d = new int[n][n];
        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n; j++) {
                d[i][j] = reader.getInt();
            }
        }

        // ----- build the model ---

        Solver cp = makeSolver();
        IntVar[] x = makeIntVarArray(cp, n, n);

        cp.post(allDifferent(x));
        
        Pair[] pairs = new Pair[n*n-n];
        int k = 0;
        for (int i = 0; i < n; i++) {
        	for (int j=0; j < n; j++) {
        		if(i != j) {
        			pairs[k] = new Pair(i,j);
        			k++;
        		}
        	}
        }

        DFSearch dfs = makeDfs(cp,
                selectMin(pairs,
                        p -> x[p.i].getSize() > 1, // filter, unbound
                        p -> -w[p.i][p.j],         // variable selector, maximum weight w[i][j] 
                        p -> { 					   // value selector, on the left branch, place on the location which is the closest possible to another location possible for facility j. On the right branch remove this value
                        	int[] valuesi = new int[n];
                    		int sizei = x[p.i].fillArray(valuesi);
                    		int[] valuesj = new int[n];
                    		int sizej = x[p.j].fillArray(valuesj);
                    		int location = x[p.i].getMin();
                    		int minDist = Integer.MAX_VALUE;
                        	for(int i=0; i<sizei; i++) {
                        		for(int j=0; j<sizej; j++) {
                        			int newDist = d[valuesi[i]][valuesj[j]];
                        			if(newDist<minDist && valuesi[i] != valuesj[j]) {
                        				minDist=newDist;
                        				location=valuesi[i]; 
                        			}
                        		}
                        	}
                        	final int loc = location;
							return branch(() -> equal(x[p.i],loc),
                                    () -> notEqual(x[p.i],loc));
                        }
                ));


        // build the objective function
        IntVar[] weightedDist = new IntVar[n*n];
        int ind = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                weightedDist[ind] = mul(element(d,x[i],x[j]),w[i][j]);
                ind++;
            }
        }
        IntVar objective = sum(weightedDist);
        cp.post(minimize(objective,dfs));



        dfs.onSolution(() -> System.out.println("objective:"+objective.getMin()));

        SearchStatistics stats = dfs.start();

        System.out.println(stats);

    }

}