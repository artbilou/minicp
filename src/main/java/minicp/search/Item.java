package minicp.search;


public class Item {
	
	public Alternative alt;
	public boolean lastAlt;
	
	public Item(Alternative alt, boolean lastAlt) {
		this.alt = alt;
		this.lastAlt = lastAlt;
	}

}
