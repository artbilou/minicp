package minicp.reversible;

public class ReversibleSparseBitSet {
	
	private int [] index;
	private ReversibleInt size;
	public ReversibleLong [] words;
	private long [] mask;
	
	// n number of elements
	public ReversibleSparseBitSet(Trail rs, int n) {
		int p = (int) Math.ceil((double)(n)/64.0); // number of word
		index = new int [p];
		mask = new long[p];
		size = new ReversibleInt(rs, p-1);
		words = new ReversibleLong[p];
		for(int i=0; i<p-1; i++) {
			words[i] = new ReversibleLong(rs, Long.MAX_VALUE);
			index[i] = i;
		}
		
		
		words[p-1] = new ReversibleLong(rs, 0);
		index[p-1] = p-1;
		for(int i=0; i<(n-(p-1)*64); i++) {
			words[p-1].setValue(words[p-1].getValue() | (1L << i)); // set the bit i
		}
	}
	
	public boolean isEmpty() {
		return size.getValue() == -1;
	}
	
	public void clearMask() {
		for(int i=0; i<=size.getValue(); i++) {
			int offset = index[i];
			mask[offset] = 0;
		}
	}
	
	public void reverseMask() {
		for(int i=0; i<=size.getValue(); i++) {
			int offset = index[i];
			if(mask[offset] == Long.MAX_VALUE)
				mask[offset] = 0;
			else
				mask[offset] = ~mask[offset];
		}
	}
	
	public void addToMask(long[] m) {
		for(int i=0; i<=size.getValue(); i++) {
			int offset = index[i];
			if(m.length > offset)
				mask[offset] = mask[offset] | m[offset];
		}
	}
	
	public void intersectWithMask() {
		for(int i=size.getValue(); i>=0; i--) {
			int offset = index[i];
			long w = words[offset].getValue() & mask[offset];
			if(w != words[offset].getValue()) {
				words[offset].setValue(w);
				if(w == 0) {
					index[i] = index[size.getValue()];
					index[size.getValue()] = offset;
					size.decrement();
				}
			}
		}
	}
	
	/*
	Post: returns the index of a word where the bit-set
	intersects with m, -1 otherwise
	*/
	public int intersectIndex(long[] m) {
		for(int i=0; i<=size.getValue(); i++) {
			int offset = index[i];
			if(m.length > offset && (words[offset].getValue() & m[offset]) != 0) {
				return offset;
			}
		}
		return -1;
	}
}
