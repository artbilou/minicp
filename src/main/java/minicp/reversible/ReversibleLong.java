package minicp.reversible;


public class ReversibleLong implements RevLong {
    class TrailEntryLong implements TrailEntry {
        private final long v;
        public TrailEntryLong(long v) {
            this.v = v;
        }
        public void restore()       { ReversibleLong.this.v = v;}
    }
    private Trail trail;
    private long v;
    private Long lastMagic = -1L;

    public ReversibleLong(Trail trail, long initial) {
        this.trail = trail;
        v = initial;
        lastMagic = trail.magic;
    }

    private void trail() {
        long trailMagic = trail.magic;
        if (lastMagic != trailMagic) {
            lastMagic = trailMagic;
            trail.pushOnTrail(new TrailEntryLong(v));
        }
    }

    public long setValue(long v) {
        if (v != this.v) {
            trail();
            this.v = v;
        }
        return this.v;
    }

    public long increment() { return setValue(getValue()+1);}
    public long decrement() { return setValue(getValue()-1);}
    public long getValue()  { return this.v; }

    @Override
    public String toString() {
        return ""+v;
    }
}
