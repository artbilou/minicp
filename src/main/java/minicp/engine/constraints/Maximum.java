/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */


package minicp.engine.constraints;

import minicp.engine.core.Constraint;
import minicp.engine.core.IntVar;
import minicp.util.InconsistencyException;
import minicp.util.NotImplementedException;

public class Maximum extends Constraint {

    private final IntVar[] x;
    private final IntVar y;

    /**
     * y = maximum(x[0],x[1],...,x[n])
     * @param x
     * @param y
     */
    public Maximum(IntVar[] x, IntVar y) {
        super(x[0].getSolver());
        assert (x.length > 0);
        this.x = x;
        this.y = y;
    }


    @Override
    public void post() throws InconsistencyException {
    	y.propagateOnBoundChange(this);
    	for(IntVar z : x)
    		z.propagateOnBoundChange(this);
    	propagate();
    }

    @Override
    public void propagate() throws InconsistencyException {
    	
    	int x_max = Integer.MIN_VALUE;
    	int x_min = Integer.MIN_VALUE;
    	int countOverlapY = 0;
    	IntVar over = null;
    	for(IntVar z : x) {
    		if(z.getMax() > x_max)
    			x_max = z.getMax();
    		if(z.getMin() > x_min)
    			x_min = z.getMin();
    		z.removeAbove(y.getMax());
    		if(z.getMax() >= y.getMin() && z.getMin() <= y.getMax()) {
    			countOverlapY++;
    			over = z;
    		}
    	}
    	
    	if(countOverlapY == 1) {
    		y.getSolver().post(new LessOrEqual(over, y));
    		y.getSolver().post(new LessOrEqual(y, over));
    		this.deactivate();
    	}
    	
    	y.removeAbove(x_max);
    	y.removeBelow(x_min);
    }
}
