/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.constraints;

import minicp.engine.core.Constraint;
import minicp.engine.core.IntVar;
import minicp.util.InconsistencyException;
import minicp.util.NotImplementedException;

public class Element1DVar extends Constraint {

    private final IntVar[] T;
    private final IntVar y;
    private final IntVar z;



    public Element1DVar(IntVar[] T, IntVar y, IntVar z) { //T[y]=z
        super(y.getSolver());
        this.T = T;
        this.y = y;
        this.z = z;
    }

    @Override
    public void post() throws InconsistencyException {
        y.removeBelow(0);
        y.removeAbove(T.length-1);

        for (IntVar var : T) {
            var.propagateOnDomainChange(this);
        }
        y.propagateOnDomainChange(this);
        z.propagateOnDomainChange(this);

        propagate();
    }

    public int[] getValues(IntVar x) {
        int[] x_values = new int[x.getSize()];
        x.fillArray(x_values);
        return x_values;
    }
    
    @Override
    public void propagate() throws InconsistencyException {
        int[] y_values = getValues(y);

        int min_T = Integer.MAX_VALUE;
        int max_T = Integer.MIN_VALUE;

        for (int i : y_values) {
            if (T[i].getMin() > z.getMax()) {
                y.remove(i);
            }
            if (T[i].getMax() < z.getMin()) {
                y.remove(i);
            }

        }
        
        for (int i : y_values) {
        	if (T[i].getMin() < min_T) {
                min_T = T[i].getMin();
            }

            if (T[i].getMax() > max_T) {
                max_T = T[i].getMax();
            }
        }
        
        z.removeBelow(Math.max(z.getMin(), min_T));
        z.removeAbove(Math.min(z.getMax(), max_T));

        if (y.isBound()) {
            y.getSolver().post(new LessOrEqual(T[y.getMin()], z));
            y.getSolver().post(new LessOrEqual(z, T[y.getMin()]));
        }
    }
 // methode avec support
    /*@Override
    public void post() throws InconsistencyException {
    	
    	y.removeBelow(0);
    	y.removeAbove(T.length-1);
        supportT = new ArrayList<Integer>(y.getSize());
        for(int i = 0; i<y.getSize(); i++)
        	supportT.add(0);
		
    	int[] valuesY = new int[y.getSize()];
		y.fillArray(valuesY);
    	int[] valuesZ = new int[z.getSize()];
		z.fillArray(valuesZ);
		boolean supportAssigned = false;
        
		for(int i : valuesY) {
			for(int v : valuesZ) {
				if(T[i].contains(v)) {
					supportT.set(i, v);
            		supportAssigned = true;
            		break;
				}
			}
        	if(!supportAssigned) {
        		supportT.remove(i);
        		y.remove(i);
        	}
        	supportAssigned = false;
        }
    	
		for(IntVar var : T)
			var.propagateOnDomainChange(this);
		y.propagateOnDomainChange(this);
		z.propagateOnDomainChange(this);
    }

    @Override
    public void propagate() throws InconsistencyException {
    	int[] valuesY = new int[y.getSize()];
		y.fillArray(valuesY);
    	int[] valuesZ = new int[z.getSize()];
		z.fillArray(valuesZ);
		boolean supportAssigned = false;
		
		for(int i : valuesY) {
			if(!T[i].contains(supportT.get(i))) {
				for(int v : valuesZ) {
					if(T[i].contains(v)) {
						supportT.set(i, v);
	            		supportAssigned = true;
	            		break;
					}
	        	}
	        	if(!supportAssigned) {
	        		supportT.remove(i);
	        		y.remove(i);
	        	}
	        	supportAssigned = false;
			}
		}
    }*/
}
