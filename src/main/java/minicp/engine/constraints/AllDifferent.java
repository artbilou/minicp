package minicp.engine.constraints;

import minicp.engine.core.Constraint;
import minicp.engine.core.IntVar;
import minicp.reversible.ReversibleInt;
import minicp.util.InconsistencyException;

public class AllDifferent extends Constraint {

    IntVar [] x;
    IntVar [] copyx;
    boolean isPermutation = true;
    int sizePermutation;
    ReversibleInt limitUnbound;

    public AllDifferent(IntVar ... x) {
        super(x[0].getSolver());
        this.x = x;
        this.copyx = x.clone();
        sizePermutation = x[0].getSize();
        limitUnbound = new ReversibleInt(x[0].getSolver().getTrail(), x.length);
    }

    @Override
    public void post() throws InconsistencyException {
        
    	if(x.length != sizePermutation) {
        	isPermutation = false;
        }
        
        for(int i = 0; i<x.length; i++) {
        	if(isPermutation) {
	        	if(x[i].getSize() != sizePermutation) {
	        		isPermutation = false;
	        	}
	        	else {
	        		int[] values = new int[sizePermutation];
	        		x[i].fillArray(values);
	        		for(int k=0; k<sizePermutation; k++) {
	        			if(!x[0].contains(values[k]))
	                		isPermutation = false;
	        		}
	        	}
        	}
        	
        	final int fi = i;
        	x[i].whenBind(() -> {
        		int sizeMax = 0;
            	int indexBind = 0;
            	for(int j = 0; j<limitUnbound.getValue(); j++) {
            		if(copyx[fi].equals(x[j])) {
            			indexBind = j;
            			break;
            		}
            	}
        		
            	for(int j = 0; j<limitUnbound.getValue(); j++) {
        			if(indexBind != j) 
        				x[j].remove(x[indexBind].getMin());
            		if(x[j].getSize() > sizeMax)
            			sizeMax = x[j].getSize();
        		}
        		
        		IntVar tmp = x[indexBind];
        		x[indexBind] = x[limitUnbound.getValue()-1];
        		x[limitUnbound.getValue()-1] = tmp;
        		limitUnbound.decrement();
        		
        		if(isPermutation && limitUnbound.getValue() > 0 && limitUnbound.getValue() < sizeMax)
        			throw new InconsistencyException();
        	});
        }
    }
}
