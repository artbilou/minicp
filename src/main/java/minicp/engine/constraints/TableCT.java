/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2017. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.constraints;

import minicp.engine.core.Constraint;
import minicp.engine.core.IntVar;
import minicp.reversible.ReversibleInt;
import minicp.reversible.ReversibleSparseBitSet;
import minicp.reversible.Trail;
import minicp.util.InconsistencyException;
import minicp.util.NotImplementedException;

import static minicp.cp.Factory.*;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

public class TableCT extends Constraint {
    private IntVar[] x; //variables
    private int[][] table; //the table
    //supports[i][v] is the set of tuples supported by x[i]=v
    private BitSet[][] supports;
    //residues[x][a] denotes the index of the word where a support was found for (x,a) the last time one was sought for
    private ReversibleInt[][] residues; 
    private ReversibleSparseBitSet validTuples;
    private ReversibleInt [] lastSizes; //domain size of each modified variable x
    //uninstantiated variables whose domains have been reduced
    private Set<IntVar> val; 
    //uninstantiated variables whose domains contain each at least one value for which a support must be found
    private Set<IntVar> sup; 
    private Trail rs;
    

    /**
     * Table constraint.
     * Assignment of x_0=v_0, x_1=v_1,... only valid if there exists a
     * row (v_0, v_1, ...) in the table.
     *
     * @param x     variables to constraint. x.length must be > 0.
     * @param table array of valid solutions (second dimension must be of same size as the array x)
     */
    public TableCT(IntVar[] x, int[][] table) {
        super(x[0].getSolver());
        this.x = new IntVar[x.length];
        this.table = table;
        this.rs = x[0].getSolver().getTrail();
        this.validTuples = new ReversibleSparseBitSet(rs, table.length);
        this.lastSizes = new ReversibleInt[x.length];
        this.val = new HashSet<IntVar>();
        this.sup = new HashSet<IntVar>();

        // Allocate supports
        supports = new BitSet[x.length][];
        this.residues = new ReversibleInt[x.length][];
        for (int i = 0; i < x.length; i++) {
            this.x[i] = minus(x[i],x[i].getMin()); // map the variables domain to start at 0
            supports[i] = new BitSet[x[i].getMax() - x[i].getMin() + 1];
            residues[i] = new ReversibleInt[x[i].getMax() - x[i].getMin() + 1];
            lastSizes[i] = new ReversibleInt(rs, x[i].getSize());
            for (int j = 0; j < supports[i].length; j++) {
                supports[i][j] = new BitSet();
                residues[i][j] = new ReversibleInt(rs, 0);
            }
        }

        // Set values in supports, which contains all the tuples supported by each var-val pair
        for (int i = 0; i < table.length; i++) { //i is the index of the tuple (in table)
            for (int j = 0; j < x.length; j++) { //j is the index of the current variable (in x)
                if (x[j].contains(table[i][j])) {
                    supports[j][table[i][j] - x[j].getMin()].set(i);
                    if(i>0)
                    	residues[j][table[i][j] - x[j].getMin()] = new ReversibleInt(rs, (int) Math.ceil((double)(i)/64.0)-1);
                }
            }
        }
    }
    
    public void updateTable() throws InconsistencyException {
    	for(IntVar var : val) {
    		int ind = 0;
			while(!x[ind].equals(var))
				ind++;
    		validTuples.clearMask();
    		int [] oldValues = new int[lastSizes[ind].getValue()-var.getSize()];
    		var.fillDeltaArray(oldValues, lastSizes[ind].getValue());
    		if(oldValues.length < var.getSize()) {
    			for(int val : oldValues) {
    				validTuples.addToMask(supports[ind][val].toLongArray());
    			}
    			validTuples.reverseMask();
    		}
    		else {
    			int [] values = new int[var.getSize()];
    			int size = var.fillArray(values);
    			for(int i=0; i<size; i++) {
    				validTuples.addToMask(supports[ind][values[i]].toLongArray());
    			}
    		}
    		validTuples.intersectWithMask();
    		if(validTuples.isEmpty())
    			break;
    	}
    }
    
    public void filterDomains() throws InconsistencyException {
    	for(IntVar var : sup) {
    		int ind = 0;
			while(!x[ind].equals(var))
				ind++;
    		int [] values = new int[var.getSize()];
			int size = var.fillArray(values);
			for(int i=0; i<size; i++) {
				int index = residues[ind][values[i]].getValue();
				long l;
				long [] support = supports[ind][values[i]].toLongArray();
				if(support.length <= index)
					l = 0;
				else
					l = support[index];
				if((validTuples.words[index].getValue() & l) == 0) {
					index = validTuples.intersectIndex(support);
					if(index != -1) 
						residues[ind][values[i]].setValue(index);
					else {
						var.remove(values[i]);
					}
				}
			}
			lastSizes[ind].setValue(var.getSize());
    	}
    }
    
    public void enforceGAC() throws InconsistencyException {
    	val.clear();
    	for(int i=0; i<x.length; i++) {
    		if(x[i].getSize() != lastSizes[i].getValue())
    			val.add(x[i]);
    	}
    	sup.clear();
    	for(IntVar var : x) {
    		if(var.getSize()>1)
    			sup.add(var);
    	}
    	updateTable();
    	for(IntVar var : val) {
    		int ind = 0;
			while(!x[ind].equals(var))
				ind++;
    		lastSizes[ind].setValue(var.getSize());
    	}
    	if(validTuples.isEmpty())
    		throw new InconsistencyException();
    	filterDomains();
    }

    @Override
    public void post() throws InconsistencyException {
        for (IntVar var : x)
        	var.propagateOnDomainChange(this);
        propagate(); //bug XCSP3
    }

    @Override
    public void propagate() throws InconsistencyException {
    	
    	// reversible method
    	//enforceGAC();
    	

    	// non reversible method
   
        // Bit-set of tuple indices all set to 0
        BitSet supportedTuples = new BitSet(table.length);
        supportedTuples.flip(0,table.length);

        // compute supportedTuples as
        //       supportedTuples = (supports[0][x[0].getMin()] | ... | supports[0][x[0].getMax()] ) & ... &
        //                         (supports[x.length-1][x[x.length-1].getMin()] | ... | supports[x.length-1][x[x.length-1].getMax()] )
        // "|" is the bitwise "or" method on BitSet
        // "&" is bitwise "and" method on BitSet
        for(int i = 0; i < x.length; i++) {
        	int[] values = new int[x[i].getSize()];
    		int size = x[i].fillArray(values);
    		BitSet disjunction = (BitSet) supports[i][values[0]].clone(); 
        	for(int j = 1; j<size; j++) {
        		disjunction.or(supports[i][values[j]]);
        	}
        	supportedTuples.and(disjunction);
        }

        for (int i = 0; i < x.length; i++) {
            for (int v = x[i].getMin(); v <= x[i].getMax(); v++) {
                if (x[i].contains(v)) {
                    // the condition for removing the value v from x[i] is to check if
                    // there is no intersection between supportedTuples and the support[i][v]
                	if(supportedTuples.intersects(supports[i][v]) == false)
                		x[i].remove(v);
                }
            }
        }
    }
}
