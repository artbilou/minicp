/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2017. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */


package minicp.engine.constraints;

import static minicp.cp.Factory.*;

import java.util.ArrayList;

import minicp.engine.core.Constraint;
import minicp.engine.core.IntVar;
import minicp.engine.core.Solver;
import minicp.reversible.ReversibleInt;
import minicp.util.InconsistencyException;
import minicp.util.NotImplementedException;
import static minicp.util.InconsistencyException.INCONSISTENCY;

public class Circuit extends Constraint {

	public final IntVar [] x;
	public final ReversibleInt [] dest;
	public final ReversibleInt [] orig;
	public final ReversibleInt [] lengthToDest;

	/**
	 * x represents an Hamiltonian circuit on the cities {0..x.length-1}
	 * where x[i] is the city visited after city i
	 * @param x
	 */
	public Circuit(IntVar [] x) {
		super(x[0].getSolver());
		this.x = x;
		dest = new ReversibleInt[x.length];
		orig = new ReversibleInt[x.length];
		lengthToDest = new ReversibleInt[x.length];
		for (int i = 0; i < x.length; i++) {
			dest[i] = new ReversibleInt(cp.getTrail(),i);
			orig[i] = new ReversibleInt(cp.getTrail(),i);
			lengthToDest[i] = new ReversibleInt(cp.getTrail(),0);
		}
	}

	@Override
	public void post() throws InconsistencyException {
		cp.post(allDifferent(x));
		for(int i=0; i<x.length; i++) {
			if(x[i].contains(i) && x[i].getSize()>1)
				x[i].remove(i);

			for(int k=0; k<x.length; k++) { // check loop less than n-1
				if(x[k].isBound()) {
					ArrayList<Integer> visited = new ArrayList<Integer>();
					boolean done = false;
					int length = 0;
					int current = k;
					while(!done && x[current].isBound()) {
						if(visited.contains(current))
							throw INCONSISTENCY;
						else
							visited.add(current);
						if(length != x.length-1) {
							current = x[current].getMax();
							length += 1;
						}
						else
							done = true;
					}
				}
			}

			x[i].removeAbove(x.length-1);
			x[i].removeBelow(0);

			int j = i;
			x[i].whenBind(() -> bind(j));
		}
	}

	private void bind(int i) throws InconsistencyException {
		boolean hasNext = true;
		int current = i;
		int length = 0;

		while(hasNext) {
			orig[current].setValue(orig[i].getValue());
			if (x[current].isBound() && length < x.length) {
				current = x[current].getMax();
				length += 1;
			}
			else {
				hasNext = false;
			}
		}
		if(length == x.length)
			length = 0;
		int destination = current;
		dest[i].setValue(destination);
		lengthToDest[i].setValue(length);

		boolean done = false;
		current = orig[i].getValue();
		while(!done) {
			if(current==i) {
				done = true;
			}
			else {
				dest[current].setValue(destination);
				lengthToDest[current].setValue(lengthToDest[current].getValue() + length);
				current = x[current].getMax();
			}
		}

		for(int k = 0; k<x.length; k++) {
			if(lengthToDest[k].getValue() != 0) { // avoid loop less than n-1
				if(x[dest[k].getValue()].getSize()>1) {
					//System.out.println("remove "+k+" from "+dest[k].getValue());
					x[dest[k].getValue()].remove(k);
				}
			}
		}
	}

	//test
    /*public static void main(String[] args) throws InconsistencyException {
    	Solver cp = new Solver();
    	IntVar[] x = new IntVar[5];
        for (int i = 0; i < 5; i++)
            x[i] = makeIntVar(cp,0,4);
    	Circuit c = new Circuit(x);
    	cp.post(c);
    	x[0].assign(2);
    	cp.fixPoint();
    	x[1].assign(3);
    	cp.fixPoint();
    	x[4].assign(1);
    	cp.fixPoint();
    	System.out.print("x: "+c.x[0]+" "+c.x[1]+" "+c.x[2]+" "+c.x[3]+" "+c.x[4]+"\n");
    	System.out.print("dest: "+c.dest[0]+" "+c.dest[1]+" "+c.dest[2]+" "+c.dest[3]+" "+c.dest[4]+"\n");
    	System.out.print("orig: "+c.orig[0]+" "+c.orig[1]+" "+c.orig[2]+" "+c.orig[3]+" "+c.orig[4]+"\n");
    	System.out.print("lengthToDest: "+c.lengthToDest[0]+" "+c.lengthToDest[1]+" "+c.lengthToDest[2]+" "+c.lengthToDest[3]+" "+c.lengthToDest[4]+"\n");
    }*/
}