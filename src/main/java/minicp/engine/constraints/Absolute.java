/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */

package minicp.engine.constraints;

import java.util.Arrays;
import minicp.engine.core.Constraint;
import minicp.engine.core.IntVar;
import minicp.util.InconsistencyException;

public class Absolute extends Constraint {

    private IntVar x;
    private IntVar y;

    /**
     * Build a constraint y = |x|
     *
     * @param x
     * @param y
     */
    public Absolute(IntVar x, IntVar y) {
        super(x.getSolver());
        this.x = x;
        this.y = y;
    }

    @Override
    public void post() throws InconsistencyException {
    	y.removeBelow(0);
    	x.propagateOnDomainChange(this);
    	y.propagateOnDomainChange(this);
    	propagate();
    }

    @Override
    public void propagate() throws InconsistencyException {
    	if(y.isBound() && x.getMax()<y.getMax())
             x.assign(-y.getMax());
    	if(y.isBound() && -x.getMin()<y.getMax())
    		 x.assign(y.getMax());
    	
    	x.removeAbove(y.getMax());
    	x.removeBelow(-y.getMax());
    	
    	y.removeAbove(Math.max(x.getMax(), -x.getMin()));
    	
    	if(x.getMin() > 0) {
    		y.removeBelow(x.getMin());
    	}
    	else if(x.getMax() < 0) {
    		y.removeBelow(-x.getMax());
    	}
    	else {
    		int[] x_values = new int[x.getSize()];
            x.fillArray(x_values);
            Arrays.sort(x_values);
            int i = 0;
            while(x_values[i] < 0) {
            	i++;
            }
    		y.removeBelow(x_values[i]);
    	}
    }

}
